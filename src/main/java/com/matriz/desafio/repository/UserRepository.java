package com.matriz.desafio.repository;

import com.matriz.desafio.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,String> {

    User findByUsername(String username);
}

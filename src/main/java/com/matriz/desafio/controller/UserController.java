package com.matriz.desafio.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.matriz.desafio.model.User;
import com.matriz.desafio.service.UserServiceImp;
import com.google.common.base.Preconditions;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserServiceImp userService;

    @GetMapping
    public String userTest () {
        return "Testando UserController";
    }

    @ApiOperation(value = "create a new default user")
    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody User user){
        userService.save(user);
        return new ResponseEntity<>("Usuário criado com sucesso",HttpStatus.CREATED);
    }

    @ApiOperation(value = "return a default user find by username", response = User.class)
    @GetMapping("userdetail/{username}")
    public UserDetails getUser(@RequestBody @PathVariable("username") String username) {
        return userService.loadUserByUsername(username);
    }

    @ApiOperation(value = "return a default user find by username", response = User.class)
    @GetMapping("/{username}")
    public User getUserI(@RequestBody @PathVariable("username") String username) {
        return userService.findUser(username);
    }

    @ApiOperation(value = "return a default user find by ID", response = User.class)
    @GetMapping("/profile/{id}")
    public Optional<User> getUserById(@RequestBody @PathVariable("id") String id) {
        return userService.findUserById(id);
    }

    @ApiOperation(value = "return a list of users", response = User[].class)
    @GetMapping("/users")
    public Iterable<User> getUsers() {
        return userService.findAll();
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('USER','ARTIST')")
    public void update(@RequestBody User user ) {
        Preconditions.checkNotNull(user.getId());
        userService.update(user);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") @Valid String id){
        userService.deleteById(id);
        return new ResponseEntity<String>("Usuário deletado com sucesso", HttpStatus.OK);
    }

}

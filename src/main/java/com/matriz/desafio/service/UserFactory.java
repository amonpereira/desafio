package com.matriz.desafio.service;

import com.matriz.desafio.model.User;

public class UserFactory {

    private User user;

    public User getUser() {
        return user;
    }

    public User setUser(User user) {
        this.user = user;
        return user;
    }
}
